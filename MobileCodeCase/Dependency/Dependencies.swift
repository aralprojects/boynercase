//
//  Dependencies.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 15.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import Foundation

struct Dependencies {
    let sessionProvider = URLSessionProvider()
    var favoriteManager = FavoriteManager()
}
