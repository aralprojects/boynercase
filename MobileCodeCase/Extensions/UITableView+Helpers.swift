//
//  UITableViewController+Helpers.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 15.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {

    func getVisibleCellsHeight() -> CGFloat {
        var height: CGFloat = 0
        for cell in self.visibleCells {
            height += cell.bounds.height
        }
        return height
    }

    func getVisibleCellHeight() -> CGFloat {
        return self.visibleCells.first?.bounds.height ?? 0
    }

    //This function will only return height if indexPath in visible area. Otherwise it returm 0.
    func getCellHeight(row: Int = 0, section: Int = 0) -> CGFloat {
        if let cell = self.cellForRow(at: IndexPath(row: row, section: section)) {
            return cell.bounds.height
        }
        return 0
    }

    func getHeaderHeight() -> CGFloat {
        return self.tableHeaderView?.bounds.height ?? 0
    }

}
