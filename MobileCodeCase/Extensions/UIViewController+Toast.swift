//
//  UIViewController+ToastMessage.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 14.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    func showToast(message: String, font: UIFont? = UIFont.systemFont(ofSize: 14)) {
        let toastLabel = UILabel(frame: .zero)
        toastLabel.translatesAutoresizingMaskIntoConstraints = false
        toastLabel.backgroundColor = UIColor(red: 0.00, green: 0.90, blue: 0.49, alpha: 1.00)
        toastLabel.textColor = UIColor(red: 0.10, green: 0.12, blue: 0.16, alpha: 1.00)
        toastLabel.font = font
        toastLabel.textAlignment = .center
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)

        toastLabel.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 20).isActive = true
        toastLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        toastLabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        toastLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true

        UIView.animate(withDuration: 5, delay: 0.2, options: .curveEaseInOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: { _ in
            toastLabel.removeFromSuperview()
        })
    } }
