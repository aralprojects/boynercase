//
//  Array+Helpers.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 15.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import Foundation

extension Array where Element: Hashable {
    func difference(from other: [Element]) -> [Element] {
        let thisSet = Set(self)
        let otherSet = Set(other)
        return Array(thisSet.subtracting(otherSet))
    }
}

extension Array where Element: Equatable {
    mutating func remove(object: Element) {
        guard let index = firstIndex(of: object) else {return}
        remove(at: index)
    }
}
