//
//  UIImageView+URLDownload.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 14.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {

    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
