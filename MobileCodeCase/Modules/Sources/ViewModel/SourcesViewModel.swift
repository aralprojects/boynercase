//
//  SourcesViewModel.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 13.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import Foundation

protocol SourcesViewModelDelegate: class {
    func reloadTable(models: [Source])
    func requestFailed()
}

class SourcesViewModel {

    var sources: [Source] = []

    weak var delegate: SourcesViewModelDelegate?
    private let sessionProvider: URLSessionProvider

    init(dependency: Dependencies,
         delegate: SourcesViewModelDelegate?) {
        self.sessionProvider = dependency.sessionProvider
        self.delegate = delegate

        self.getSources()
    }

    private func getSources() {
        sessionProvider.request(type: SourceRoot.self, service: SourcesService.all) { [weak self] response in

            guard let strongSelf = self else { return }

            switch response {
            case let .success(response):

                let sources = response.sources
                strongSelf.sources = sources
                strongSelf.delegate?.reloadTable(models: sources)

            case let .failure(error):
                print(error)
                strongSelf.delegate?.requestFailed()
            }
        }
    }
}
