//
//  SourcesCell.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 13.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import UIKit

class SourcesCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    static let identifier = "CellSources"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(title: String, description: String) {
        self.titleLabel.text = title
        self.descriptionLabel.text = description

        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        self.titleLabel.textColor = UIColor(red: 0.33, green: 0.69, blue: 0.97, alpha: 1.00)
    }

}
