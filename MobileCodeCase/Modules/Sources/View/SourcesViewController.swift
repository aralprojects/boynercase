//
//  SourcesViewController.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 13.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import UIKit

class SourcesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    //To-Do: This dependency struct, will move inside some Root class in future.
    private let dependency = Dependencies()
    private var viewModel: SourcesViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    public func setup() {
        viewModel = SourcesViewModel(dependency: dependency,
                                     delegate: self)

        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 600
    }
}

// MARK: - UITableView Delegates
extension SourcesViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.sources.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SourcesCell.identifier) as? SourcesCell else {
            return UITableViewCell()
        }

        guard let news = viewModel?.sources else { return UITableViewCell() }

        if news.count > 0 {
            let model = news[indexPath.row]
            cell.configure(title: model.name, description: model.description)
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let model = viewModel?.sources[indexPath.row]
        performSegue(withIdentifier: "segueToNews", sender: model)
    }
}

// MARK: - Segue
extension SourcesViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToNews" {
            // initialize new view controller and cast it as your view controller
            if let viewController = segue.destination as? ArticleViewController {
                viewController.country = (sender as? Source)?.country
                viewController.source = (sender as? Source)?.id
                viewController.dependency = self.dependency
            }
        }
    }
}

// MARK: - ViewModel Delegates
extension SourcesViewController: SourcesViewModelDelegate {
    func reloadTable(models: [Source]) {
        DispatchQueue.main.sync {
            self.tableView.reloadData()
            //self.tableView.reloadRows(at: [IndexPath(row: type, section: 0)], with: .fade)
        }
    }

    func requestFailed() {
        //To-Do! - Show some error on view
    }
}
