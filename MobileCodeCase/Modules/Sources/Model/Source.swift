//
//  News.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 13.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import Foundation

struct SourceRoot: Codable {
    let sources: [Source]
}

//swiftlint:disable identifier_name
struct Source: Codable {
    let id: String
    let name: String
    let description: String
    let url: URL
    let category: String
    let language: String
    let country: String
}
