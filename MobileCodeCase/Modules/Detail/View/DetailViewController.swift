//
//  DetailViewController.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 13.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import UIKit
import WebKit

class DetailViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    var contentTitle: String?
    var contentUrl: URL?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    func setup() {
        self.navigationItem.title = contentTitle

        if let url = contentUrl {
            let request = URLRequest(url: url)
            webView.load(request)
        }
    }
}

extension WKWebView {
    func load(_ urlString: String) {
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            load(request)
        }
    }
}
