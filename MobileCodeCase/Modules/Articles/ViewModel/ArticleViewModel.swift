//
//  NewsViewModel.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 13.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import Foundation

protocol ArticleViewModelDelegate: class {
    func reloadTable()
    func updateTable(count: Int)
    func requestFailed()
}

class ArticleViewModel {

    var articles: [Article] = []
    var newArticles: [Article] = []
    var filteredArticles: [Article] = []

    weak var delegate: ArticleViewModelDelegate?
    private let sessionProvider: URLSessionProvider
    private var country = ""
    private var source = ""
    private var timer = Timer()

    init(dependency: Dependencies,
         country: String,
         source: String,
         delegate: ArticleViewModelDelegate?) {

        self.sessionProvider = dependency.sessionProvider
        self.delegate = delegate
        self.country = country
        self.source = source

        self.timer = Timer.scheduledTimer(timeInterval: 60,
                                          target: self,
                                          selector: #selector(refresh),
                                          userInfo: nil,
                                          repeats: true)
        self.getArticles(isRefresh: false)
    }

    @objc func refresh() {
        if isFilteringActive() == false {
            getArticles(isRefresh: true)
        }
    }

    private func getArticles(isRefresh: Bool) {
        sessionProvider.request(type: ArticleRoot.self,
                                service: HeadlinesService.all("", source)) { [weak self] response in

            guard let strongSelf = self else { return }

            switch response {
            case let .success(response):

                strongSelf.newArticles = response.articles
                if isRefresh {
                    strongSelf.addDummyArticlesForTesting()
                }
                strongSelf.updateTableView()

            case let .failure(error):
                print(error)
                strongSelf.delegate?.requestFailed()
            }
        }
    }

    private func updateTableView() {

        let countOfAddedItems = newArticles.count - articles.count

        //If we’re adding information
        if newArticles.count > 0, newArticles.count != articles.count {
            #if DEBUG
                print("We have a difference in counts of \(countOfAddedItems)")
            #endif

            if articles.count != 0 {
                //Update data model
                articles = newArticles
                delegate?.updateTable(count: countOfAddedItems)

            } else {
                #if DEBUG
                    print("There was no data in oldArray, avoid scrolling.")
                #endif

                //Update data model
                articles = newArticles
                delegate?.reloadTable()
            }
        }
    }

    // MARK: - Filtering
    public func filterArticles(searchText: String) {
        filteredArticles = articles.filter({
            $0.title?.range(of: searchText, options: .caseInsensitive) != nil
        })
     }

    public func isFilteringActive() -> Bool {
        return self.filteredArticles.count != 0 ? true : false
    }

    // MARK: - Helpers
    public func getFormattedDate(of model: Article) -> String {

        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"

        if let dateString = model.publishedAt, let date = formatter.date(from: dateString) {
            formatter.dateFormat = "EEEE, d MMM - HH:mm"
            return formatter.string(from: date)
        }

        return model.publishedAt ?? ""
    }

    //To-do!
    //This is for only testing purpose! In future it will move to Unit testing..
    //swiftlint:disable all
    fileprivate func addDummyArticlesForTesting() {

        let dummy1 = Article(source: Article.Source(id: "test1", name: "Test Name"),
                             author: "Goktug Aral",
                             title: "Bla Bla Bla-1",
                             description: "Avengers star stepped down from playing trans male role in 2018",
                             url: URL(string:"https://www.independent.co.uk/arts-entertainment/films/news/scarlett-johansson-avengers-endgame-rub-and-tug-trans-role-ghost-in-the-shell-whitewashing-diversity-a9003991.html"),
                             urlToImage: URL(string: "https://static.independent.co.uk/s3fs-public/thumbnails/image/2019/04/23/07/gettyimages-1138777533.jpg"),
                             publishedAt: "2019-07-14T07:44:00Z",
                             content: nil)

        let dummy2 = Article(source: Article.Source(id: "test2", name: "Test Name 2"),
                             author: "Goktug Aral",
                             title: "Bla Bla Bla-2",
                             description: "Avengers star stepped down from playing trans male role in 2018",
                             url: URL(string:"https://www.independent.co.uk/arts-entertainment/films/news/scarlett-johansson-avengers-endgame-rub-and-tug-trans-role-ghost-in-the-shell-whitewashing-diversity-a9003991.html"),
                             urlToImage: URL(string: "https://static.independent.co.uk/s3fs-public/thumbnails/image/2019/04/23/07/gettyimages-1138777533.jpg"),
                             publishedAt: "2019-07-14T07:44:00Z",
                             content: nil)

        let dummy3 = Article(source: Article.Source(id: "test2", name: "Test Name 2"),
                             author: "Goktug Aral",
                             title: "Bla Bla Bla-3",
                             description: "Avengers star stepped down from playing trans male role in 2018",
                             url: URL(string:"https://www.independent.co.uk/arts-entertainment/films/news/scarlett-johansson-avengers-endgame-rub-and-tug-trans-role-ghost-in-the-shell-whitewashing-diversity-a9003991.html"),
                             urlToImage: URL(string: "https://static.independent.co.uk/s3fs-public/thumbnails/image/2019/04/23/07/gettyimages-1138777533.jpg"),
                             publishedAt: "2019-07-14T07:44:00Z",
                             content: nil)

        self.newArticles.insert(dummy1, at: 0)
        self.newArticles.insert(dummy2, at: 1)
        self.newArticles.insert(dummy3, at: 2)
    }
}

