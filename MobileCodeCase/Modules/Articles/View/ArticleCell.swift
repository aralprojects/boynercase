//
//  NewsCell.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 13.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import UIKit
import Kingfisher

class ArticleCell: UITableViewCell {

    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var addToListButton: UIButton!

    static let identifier = "CellArticle"
    var favoriteButtonHandler: ((Bool) -> Void)?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

//    override func prepareForReuse() {
//        super.prepareForReuse()
//        self.titleLabel.text = ""
//        self.dateLabel.text = ""
//        self.articleImageView.image = nil
//    }

    func configure(title: String?, date: String?, url: URL?, isFavorited: Bool = false) {
        self.titleLabel.text = title ?? ""
        self.dateLabel.text = date ?? ""

        self.titleLabel.font = UIFont.boldSystemFont(ofSize: 20)
        self.titleLabel.textColor = UIColor(red: 0.33, green: 0.69, blue: 0.97, alpha: 1.00)

        if let url = url {
            self.articleImageView.kf.setImage(with: ImageResource(downloadURL: url),
                                              placeholder: UIImage(named: "bg_placeholder"))
        } else {
             self.articleImageView.image = UIImage(named: "bg_placeholder")
        }

        updateFavoriteButtonState(isFavorited: isFavorited)
    }

    func updateFavoriteButtonState(isFavorited: Bool) {
        let image = UIImage(named: "icon_add")?.withRenderingMode(.alwaysTemplate)
        addToListButton.setImage(image, for: .normal)
        if isFavorited {
            addToListButton.tintColor = UIColor(red: 0.00, green: 0.90, blue: 0.49, alpha: 1.00)
        } else {
            addToListButton.tintColor = UIColor(red: 0.57, green: 0.58, blue: 0.60, alpha: 1.00)
        }
    }

    @IBAction func addToListButtonClicked(_ sender: Any) {
        favoriteButtonHandler?(true)
    }
}
