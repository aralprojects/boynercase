//
//  NewsListViewController.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 13.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import UIKit

class ArticleViewController: UIViewController {

    var country: String?
    var source: String?
    var dependency: Dependencies?

    private var viewModel: ArticleViewModel?

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    public func setup() {
        guard let dependency = self.dependency else {
            print("Error - Check your dependency injection!")
            return
        }

        guard let country = self.country else {
            print("Error - Check your country code!")
            return
        }

        guard let source = self.source else {
            print("Error - Check your source code!")
            return
        }

        searchBar.delegate = self
        searchBar.returnKeyType = .done
        searchBar.placeholder = "Search some article..."

        viewModel = ArticleViewModel(dependency: dependency,
                                     country: country,
                                     source: source,
                                     delegate: self)

        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 600
        tableView.keyboardDismissMode = .onDrag
    }
}

// MARK: - UITableView Delegates
extension ArticleViewController: UITableViewDataSource, UITableViewDelegate {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        guard let viewModel = viewModel else {
            return 0
        }

        if viewModel.isFilteringActive() {
            return viewModel.filteredArticles.count
        } else {
            return viewModel.articles.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ArticleCell.identifier) as? ArticleCell else {
            return UITableViewCell()
        }
        guard let viewModel = viewModel else { return UITableViewCell() }

        let articles = viewModel.isFilteringActive() ? viewModel.filteredArticles : viewModel.articles
        let model = articles[indexPath.row]

        //Check if article is in favorited list via FavoriteManager.
        let isFavorited = dependency?.favoriteManager.checkArticleIsExist(article: model) ?? false

        //If there are some articles, start configure the cell.
        if articles.count > 0 {
            cell.configure(title: model.title,
                           date: viewModel.getFormattedDate(of: model),
                           url: model.urlToImage,
                           isFavorited: isFavorited)

            //Catch favorite button click
            cell.favoriteButtonHandler = { [weak self] _ in
                guard let strongSelf = self else { return }
                // If is already favorited, then we should remove from our list and refresh cell UI.
                if isFavorited {
                    strongSelf.dependency?.favoriteManager.removeFrom(article: model)
                    cell.updateFavoriteButtonState(isFavorited: false)
                } else {
                    strongSelf.dependency?.favoriteManager.addTo(article: model)
                    cell.updateFavoriteButtonState(isFavorited: true)
                }
                
            }
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let model = viewModel?.articles[indexPath.row]
        performSegue(withIdentifier: "segueToDetail", sender: model)
    }
}

// MARK: - Segue
extension ArticleViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToDetail" {
            // initialize new view controller and cast it as your view controller
            if let viewController = segue.destination as? DetailViewController {
                viewController.contentUrl = (sender as? Article)?.url
                viewController.contentTitle = (sender as? Article)?.source.name
            }
        }
    }
}

// MARK: - UISearchBarDelegate
extension ArticleViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
         searchBar.resignFirstResponder()
    }

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel?.filterArticles(searchText: searchText)
        tableView.reloadData()
    }

}

// MARK: - ViewModel Delegates
extension ArticleViewController: ArticleViewModelDelegate {

    func updateTable(count: Int) {

        DispatchQueue.main.sync {

            //1) Detect and store height

            let topWillBeAt = getTopVisibleRow() + count
            let oldHeightDifference = heightDifference()

            //2) Insert new indexpath
            var index = 0
            var paths = [IndexPath]()
            while index < count {
                paths.append(IndexPath(row: index, section: 0))
                index += 1
            }
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: paths, with: .none)
            self.tableView.endUpdates()
             self.tableView.layer.removeAllAnimations()
            //3) a.If you close to top of the screen, then you can see default insert animation.
            //   b.If you scroll down, while update received you will see toast message.
            //   offset would keep its position.
            if (tableView.contentOffset.y - tableView.getVisibleCellHeight()) >= 0 {
                showToast(message: "New articles has been added")
                tableView.scrollToRow(at: IndexPath(row: topWillBeAt, section: 0), at: .none, animated: false)
                tableView.contentOffset.y -= oldHeightDifference
            }
        }
    }

    func reloadTable() {
        DispatchQueue.main.sync {
            self.tableView.reloadData()
        }
    }

    func requestFailed() {
        //To-Do! - Show some error on view
    }
}

// MARK: - Helpers
extension ArticleViewController {
    func getTopVisibleRow() -> Int {
        //We need this to accounts for the translucency below the nav bar
        let navBar = navigationController?.navigationBar
        let navBarPoistion = tableView.convert(navBar!.bounds, from: navBar)
        let pointWhereNavBarEnds = CGPoint(x: 0,
                                           y: navBarPoistion.origin.y + navBarPoistion.size.height + 1)
        let accurateIndexPath = tableView.indexPathForRow(at: pointWhereNavBarEnds)
        return accurateIndexPath?.row ?? 0
    }

    func heightDifference() -> CGFloat {
        let rectForTopRow = tableView.rectForRow(at: IndexPath(row: getTopVisibleRow(), section: 0))
        let navBar = navigationController?.navigationBar
        let whereIsNavBarInTableView = tableView.convert(navBar!.bounds, from: navBar)
        let pointWhereNavBarEnds = CGPoint(x: 0,
                                           y: whereIsNavBarInTableView.origin.y + whereIsNavBarInTableView.size.height)
        let differenceBetweenTopRowAndNavBar = rectForTopRow.origin.y - pointWhereNavBarEnds.y
        return differenceBetweenTopRowAndNavBar
    }
}
