//
//  Article.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 13.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import Foundation

struct ArticleRoot: Codable {
    let articles: [Article]
}

struct Article: Codable, Hashable {
    static func == (lhs: Article, rhs: Article) -> Bool {
        return lhs.author == rhs.author && lhs.title == rhs.title
    }

    //swiftlint:disable identifier_name
    struct Source: Codable, Hashable {
        let id: String?
        let name: String?
    }

    let source: Source
    let author: String?
    let title: String?
    let description: String?
    let url: URL?
    let urlToImage: URL?
    let publishedAt: String?
    let content: String?
}
