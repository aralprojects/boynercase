//
//  FavoriteManager.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 15.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import Foundation

//A helping manager for add favorite article to UserDefaults.
struct FavoriteManager {
    init() {}

    /*
      https://newsapi.org/docs/endpoints/top-headlines service result would not give you an unique id for article.
      There is a source struct in article which occurs from id and name but these are not unique values.

      For performance, I do not want to store whole object on UserDefaults.
      That is why I created an unique combination for article which is made with (source.id + title).
     */

    //To add an article
    mutating func addTo(article: Article) {
        let unique = (article.source.id ?? "") + (article.title ?? "")
        if var caches = UserDefaults.standard.object(forKey: "favorites") as? [[String: Bool]] {
            caches.append([unique: true])
            UserDefaults.standard.set(caches, forKey: "favorites")
        } else {
            let caches = [[unique: true]]
            UserDefaults.standard.set(caches, forKey: "favorites")
        }
    }

    //To remove an article
    func removeFrom(article: Article) {
        let unique = (article.source.id ?? "") + (article.title ?? "")

         if var caches = UserDefaults.standard.object(forKey: "favorites") as? [[String: Bool]] {

            if let cache = caches.first(where: {$0.keys.contains(unique)}) {
                caches.remove(object: cache)
            }
            UserDefaults.standard.set(caches, forKey: "favorites")
        }
    }

    //Check if an article exist
    func checkArticleIsExist(article: Article) -> Bool {
        let unique = (article.source.id ?? "") + (article.title ?? "")
        if let caches = UserDefaults.standard.object(forKey: "favorites") as? [[String: Bool]] {
            return caches.contains { $0.keys.contains(unique) }
        }
        return false
    }

}

// MARK: - Helpers
extension UserDefaults {
    func resetDefaults() {
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        UserDefaults.standard.synchronize()
    }
}
