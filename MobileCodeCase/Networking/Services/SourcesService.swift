//
//  PostService.swift
//  NetworkLayer
//
//  Created by Marcin Jackowski on 06/09/2018.
//  Copyright © 2018 CocoApps. All rights reserved.
//

import Foundation
let apiKey = "df89b23ca2184b7f86c47d3d2469e23f"

enum SourcesService: ServiceProtocol {
    case all

    var baseURL: URL {
        return URL(string: "https://newsapi.org/v2/")!
    }

    var path: String {
        switch self {
        case .all:
            return "sources"
        }
    }

    var method: HTTPMethod {
        return .get
    }

    var task: Task {
        switch self {
        case .all:
            let parameters = ["apiKey": apiKey]
            return .requestParameters(parameters)
        }
    }

    var headers: Headers? {
        return nil
    }
    
    var parametersEncoding: ParametersEncoding {
        return .url
    }
}
