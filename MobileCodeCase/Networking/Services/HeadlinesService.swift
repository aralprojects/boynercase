//
//  HeadlinesService.swift
//  MobileCodeCase
//
//  Created by Göktuğ Aral on 13.07.2019.
//  Copyright © 2019 Göktuğ Aral. All rights reserved.
//

import Foundation

enum HeadlinesService: ServiceProtocol {

    case all(String, String)

    var baseURL: URL {
        return URL(string: "https://newsapi.org/v2/")!
    }

    var path: String {
        switch self {
        case .all:
            return "top-headlines"
        }
    }

    var method: HTTPMethod {
        return .get
    }

    var task: Task {
        switch self {
        case let .all(country, source):
            let parameters = ["country": country,
                              "sources": source,
                              "apiKey": apiKey]
            return .requestParameters(parameters)
            //        case let .comments(postId):
            //            let parameters = ["postId": postId]
            //            return .requestParameters(parameters)
        }
    }

    var headers: Headers? {
        return nil
    }

    var parametersEncoding: ParametersEncoding {
        return .url
    }
}
